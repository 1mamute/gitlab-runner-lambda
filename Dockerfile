FROM public.ecr.aws/lambda/go:latest

ENV REGISTRATION_TOKEN=MGNzUWEyMxswe4vtmaEg

COPY gitlab-runner/gitlab-runner ${LAMBDA_TASK_ROOT}

WORKDIR ${LAMBDA_TASK_ROOT}

RUN chmod +x ./gitlab-runner

ENV USER=root

#RUN ./gitlab-runner install --user=$USER
#RUN ./gitlab-runner install --user=$USER --working-directory=${LAMBDA_TASK_ROOT}/gitlab-runner

RUN ./gitlab-runner register --non-interactive --url 'https://gitlab.com' --registration-token $REGISTRATION_TOKEN --executor 'shell'

#RUN export TOKEN=$(cat /etc/gitlab-runner/config.toml | grep -i "token" | awk -F '"' '{print $2}')
#RUN echo $TOKEN

#RUN ./gitlab-runner run-single --url 'https://gitlab.com' --token $TOKEN --executor 'shell'

ENTRYPOINT ["${LAMBDA_TASK_ROOT}/./gitlab-runner", "run-single", "--url", "https://gitlab.com", "--token", "\$(cat /etc/gitlab-runner/config.toml | grep -i 'token' | awk -F '\"' '{print \$2}')", "--executor", "shell"]
