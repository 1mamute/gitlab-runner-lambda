resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "lambda_gitlab_runner" {
  function_name = "lambda_gitlab_runner"
  role          = aws_iam_role.iam_for_lambda.arn
  image_uri     = ""
  package_type  = "Image"
  memory_size   = 256
}

output "lambda_name" {
  value = aws_lambda_function.lambda_gitlab_runner.id
}
